# README

# Blog App

A simple app where users can signup and post blogs and commment on them. Also view other users blogs and comment on it.

## Environment

* Ruby version : 2.4.1

* Rails version : 5.0.3

* Postgresql

* System dependencies
  - nodejs

* Configuration
  1. create config/secrets.yml

  copy below content and add a secret key

      development:
        secret_key_base:
      test:
        secret_key_base:

  2. create config/application.yml

      # Add configuration values

        development:
          database_host: ''
          database_name: ''
          database_username: ''
          database_password: ''

        test:
          database_host: ''
          database_name: ''
          database_username: ''
          database_password: ''

* Database creation

  run the db create commands

      $ rake db:create RAILS_ENV=test

      $ rake db:create RAILS_ENV=development

  then run the seed file

      $ rake db:seed
