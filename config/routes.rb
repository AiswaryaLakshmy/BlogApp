Rails.application.routes.draw do
  root 'blogs#index'
  resources :blogs do
    resources :comments
  end
  devise_for :users, :controllers => { registrations: 'registrations' }
  get ':id/my_blog' => 'blogs#my_blog', as: :my_blog
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
